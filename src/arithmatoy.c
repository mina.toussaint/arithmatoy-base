#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }
  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  
  int len_lhs = strlen(lhs);
  int len_rhs = strlen(rhs);
  int max_len = len_lhs > len_rhs ? len_lhs : len_rhs;
  char *result = (char *)malloc(max_len + 2); // +2 cause 1 for possible carry and another one for null terminator
  if (!result) {
    return NULL; 
  }

  int carry = 0;

  int i = len_lhs - 1;
  int j = len_rhs - 1;
  int k = max_len;

  result[k + 1] = '\0'; // add a \0 for the end of the sting

  while (i >= 0 || j >= 0 || carry) {
    int digit1 = i >= 0 ? get_digit_value(lhs[i]) : 0;
    int digit2 = j >= 0 ? get_digit_value(rhs[j]) : 0;
    i--;
    j--;
    int sum = digit1 + digit2 + carry;
    carry = sum / base;
    result[k--] = to_digit(sum % base);

    if (VERBOSE) {
        fprintf(stderr, "add: digit %d digit %d carry %d\n", digit1, digit2, carry);
        fprintf(stderr, "add: result: digit %c carry %d\n", result[k + 1], carry);
    }
}


  // if the result has a leading zero, we remove it
  if (k == 0) {
    memmove(result, result + 1, max_len + 1);
  }

  result = drop_leading_zeros(result);

   if (VERBOSE) {
    fprintf(stderr, "add: final carry %s\n", carry);
}
  return result;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  if (compare_strings(lhs, rhs) < 0) {
      return NULL;
    }

  // You should allocate a new char* large enough to store the result as a
  int len_lhs = strlen(lhs);
  int len_rhs = strlen(rhs);
  int max_len = len_lhs > len_rhs ? len_lhs : len_rhs;
  char *result = (char *)malloc(max_len + 1); // +1 for null terminator
  if (!result) {
    return NULL; 
  }

  int carry = 0;
  int i = len_lhs - 1;
  int j = len_rhs - 1;

  int k = max_len - 1;
  result[max_len] = '\0';
  
  while (i >= 0 || j >= 0) {
    int digit1 = (i >= 0) ? get_digit_value(lhs[i]) : 0;
    int digit2 = (j >= 0) ? get_digit_value(rhs[j]) : 0;
    i--;
    j--;
    int diff = digit1 - digit2 - carry;

    if (diff < 0) {
      diff += base;
      carry = 1;
    } else {
      carry = 0;
    }

    result[k--] = to_digit(diff);

    if (VERBOSE) {
      fprintf(stderr, "sub: digit %d digit %d carry %d\n", lhs[i], rhs[j], carry);
      fprintf(stderr, "sub: result: digit %c carry %d\n", result[k + 1], carry);
    }
  }

  result = drop_leading_zeros(result);
  return result;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }
  // Fill the function, the goal is to compute lhs * rhs

  // You should allocate a new char* large enough to store the result as a
  int len_lhs = strlen(lhs);
  int len_rhs = strlen(rhs);
  int max_len = len_lhs + len_rhs;
  char *result = (char *)malloc(max_len + 2); // +1 for possible carry, +1 for null terminator
  if (!result) {
    return NULL; 
  }

  // Beginning of the multiplication
  // Loop through the digits of lhs one by one and multiply it with all the digits of rhs
  for (int i = 0; i < len_lhs; i++) {
    int carry = 0;
    int digit1 = get_digit_value(lhs[len_lhs - 1 - i]);
    if (VERBOSE) {
      fprintf(stderr, "mul: digit %d number %s\n", digit1, rhs);
    }

    // Loop through the digits of rhs one by one and mutiply it with the current digit of lhs
    for (int j = 0; j < len_rhs; j++) {
      int digit2 = get_digit_value(rhs[len_rhs - 1 - j]);
      int sum = result[i + j] + digit1 * digit2 + carry;
      result[i + j] = sum % base;
      carry = sum / base;
      if (VERBOSE) {
        fprintf(stderr, "mul: digit %d digit %d carry %d\n", digit1, digit2, carry);
        fprintf(stderr, "mul: result: digit %d carry %d\n", result[i + j + 1], carry);
      }
    }

    // Is there a carry left? If yes, add it to the next digit
    if (carry > 0) {
      result[i + len_rhs] += carry;
    }
    if (VERBOSE) {
      fprintf(stderr, "mul: final carry %d\n", carry);
    }
  }

  // Reduce the size of the result if there are leading zeros
  int result_len = max_len;
  while (result_len > 1 && result[result_len - 1] == 0) {
    result_len--;
  }

  // Enter the result in a string
  char *result_str = malloc(result_len + 1);
  for (int i = 0; i < result_len; i++) {
    result_str[result_len - 1 - i] = to_digit(result[i]);
  }

  result_str[result_len] = '\0';

  if (VERBOSE) {
    fprintf(stderr, "mul: result %s\n", result_str);
}
  return result_str;
}

// Here are some utility functions that might be helpful to implement add, sub and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}

int compare_strings(const char *lhs, const char *rhs) {
  // Allow to compare two strings, ignoring leading zeros
  // Helpul to see if lhs is smaller than rhs in sub
    lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);
  int len_lhs = strlen(lhs);
  int len_rhs = strlen(rhs);

  if (len_lhs != len_rhs) {
    return len_lhs - len_rhs;
  }

  for (int i = 0; i < len_lhs; i++) {
    if (lhs[i] != rhs[i]) {
      return lhs[i] - rhs[i];
    }
  }
  return 0;
}